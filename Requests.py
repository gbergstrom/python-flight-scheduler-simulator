import Queue
import time

"""The purpose of this class is to take in requests for flights taking off. The request will be read in via a text file
    and then have each value saved into a specifica variable. Once successfully saved each request will be saved to its
    own object and then stored within a list of objects.   """


class Requests(object):
    flight_id = ""  # The flight id
    sub = 0  # The submission time for the request
    slot = 0  # The slot time request
    length = 0  # The length of time needed for takeoff
    start = 0  # The actual start time of the takeoff
    end = 0  # The actual end of the takeoff

    """This initializes the request object where each value of the request is stored into 
        a variable that will be used at some point within the scheduling process.  """

    def __init__(self, id, sub, slot, length, start, end):
        self.id = id  # The flight id
        self.sub = sub  # The submission time for the request
        self.slot = slot  # The slot time request
        self.length = length  # The length of time needed for takeoff
        self.actual_start = start  # The actual start time of the takeoff
        self.actual_end = end  # The actual end of the takeoff

    def __cmp__(self, other):
        return cmp(self.slot, other.slot)


lines = []  # This list will take in each line of instructions from the text file.
file_input = []  # This list will take the lines read from the lines[] list and assign each value to a variable
request_list = []  # This list will be where each of the request objects will be stored.
q = Queue.PriorityQueue()  # This initializes the priority queue for the scheduler

# loops through the text file line by line and places each line into a list
with open("/Users/gabe/PycharmProjects/FlightScheduler/Test.txt") as file:
    for line in file:
        line = line.strip().replace("\n", "").split(",")
        lines.append(line)
i = 0

"""This loop is for taking each of the instructions from a request and placing them in variable, which is then
    placed into a request object. Each of the request objects are then placed into the request_list """

while i < len(lines):
    file_input = lines[i]
    flight_id = file_input[0]
    sub = int(file_input[1])
    slot = int(file_input[2])
    length = int(file_input[3])
    start = 0
    end = 0
    request = Requests(flight_id, sub, slot, length, start, end)
    request_list.append(request)
    q.put(Requests(request_list[i].id, request_list[i].sub, request_list[i].slot, request_list[i].length,
                   request_list[i].start, request_list[i].end))
    i = i + 1

timer = 0  # variable for keeping track of the passage of time
current_schedule = ""  # string variable for outputting the status of the queue
submission_times = []  # list of all the submissions in order
queue_status = []  # list of the status of the queue, this printed to show the progression of the queue
final_status = []

"""This section is where the queue is taken and each element of the queue is then pop into a list which 
    then is compared to the current time to see if it should be added to the time_status list. As the time 
    loop progresses more and more flights will be added to the list and as flights take off they will be removed 
    from the list. This will continue until all flights have taken off. """

print ("AIRSTRIP TIMESLOT SIMULATOR")
"""Setting up virtual clock"""
seconds = 0

while True:
    print (("{:02}:{:02}:{:02} ".format(seconds//3600, seconds%3600//60, seconds%60)) + time.strftime('%x %Z'))
    time.sleep(1)
    counter = int(seconds%3600//60)
    timer = 0
    for i in range(len(q.queue)):
        if q.queue[timer].sub == counter:
            cur_task = q.queue[timer]
            q.queue[timer].actual_start = counter
            q.queue[timer].actual_end = q.queue[timer].length + q.queue[timer - 1].actual_end
            queue_status.append(str(cur_task.id) + "(started at " + str(counter) + ")")
            timer = timer + 1
        else:
            timer = timer + 1

    seconds = seconds + 60

    timer = 0
    for i in range(len(queue_status)):
        if q.queue[timer].actual_end == counter:
            queue_status.pop(0)
            timer = timer + 1
        else:
            timer = timer + 1
    print queue_status
    print("\r")

