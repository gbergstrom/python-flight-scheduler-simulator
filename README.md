This project uses the Python programming language to simulate aircraft take off time slots at an airport.
The program will keep track of the information needed to schedule the airstrip resource: request identifier, 
request submission time, time slot requested, length of time requested, actual start time, actual end time.
This will be done by creating a class that loops through a text file of requests and stores the necessary information.
The program will then have a loop simulating the passing of time and will be adding flights to a queue based off of their 
submission time. As flights are added, flights that have an earlier request slot time than others will be moved up in the queue
and the later flights will be pushed back. As the time loop progresses the status of the queue will be outputted, displaying the 
current order of the flights scheduled at that given time. As flights take off they will be removed from the queue. This 
process will continue until all flights have taken off.
